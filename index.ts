import { config } from "dotenv";
import { app } from "./src/app";

config();

const PORT = process.env.PORT ?? 3333

app.listen(PORT, () => console.log(`Koa listening in ${PORT}`));
