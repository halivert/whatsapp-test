import { defineConfig } from "rollup";

import json from "@rollup/plugin-json";
import commonJS from "@rollup/plugin-commonjs";
import terser from "@rollup/plugin-terser";
import typescript from "@rollup/plugin-typescript";
import nodeResolve from "@rollup/plugin-node-resolve";

export default defineConfig((config) => {
	const plugins = [
		typescript(),
		nodeResolve(),
		commonJS(),
		json(),
		config.configDev ? "" : terser(),
	];

	return {
		input: ["index.ts"],
		plugins,
		output: {
			dir: "dist",
			format: "esm",
			sourcemap: config.configDev,
		}
	}
})

