import { Context, Next } from "koa";

export function errorHandler() {
	return async (ctx: Context, next: Next) => {
		try {
			await next();
		} catch (err: unknown) {
			if (err instanceof Error) {
				ctx.status = 500;
				ctx.body = {
					message: err.message,
				};
			}
		}
	};
}
