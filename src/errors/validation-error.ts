export class ValidationError extends Error {
	status: number;
	body: Record<string, string>;

	constructor(message: string, field: string) {
		super(message);
		this.status = 400;
		this.body = { message, field };
	}
}
