import Koa from "koa"
import { errorHandler } from "./errors"
import cors from "@koa/cors"
import { whatsappRouter } from "./routes/whatsapp"

const app = new Koa()

app.use(
	cors({
		allowMethods: ["GET", "PUT", "POST", "DELETE"],
	})
)

app.use(errorHandler())

app.use(whatsappRouter);

export { app }

