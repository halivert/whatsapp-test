export interface Message {
	to: string
	text: string
	context?: {
		message_id: string
	}
}
