import { Message } from "../models";

export async function sendMessage(message: Message) {
	const phone = process.env.PHONE_ID;
	const token = process.env.TOKEN;

	if (!phone || !token) throw new Error("Env not set");

	const response = await fetch(
		`https://graph.facebook.com/v15.0/${phone}/messages`,
		{
			method: "POST",
			headers: new Headers({
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`,
			}),
			body: JSON.stringify({
				messaging_product: "whatsapp",
				to: message.to,
				text: {
					body: message.text,
				},
			}),
		}
	);

	return await response.json();
}

