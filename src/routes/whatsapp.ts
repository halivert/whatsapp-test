import { Context } from "koa";
import Router from "koa2-router";
import { subtle } from "crypto";

const router = new Router();

router.get("/", async function (ctx: Context) {
	const data = ctx.query

	function error() {
		ctx.status = 403;
		ctx.body = "Forbidden";
	}

	if (data['hub.mode'] !== 'subscribe') {
		return error();
	}

	if (process.env.SECRET !== data['hub.verify_token']) {
		return error();
	}

	ctx.status = 200
	ctx.body = data['hub.challenge']
});

router.post('/', async function (ctx: Context) {
	// subtle.digest('SHA-256', );
});

export { router as whatsappRouter };
